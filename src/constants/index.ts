export const WS_SEND_JOIN: string = 'ws://join';
export const WS_RECEIVE_JOIN: string = 'ws://join';
export const WS_SEND_OFFER: string = 'ws://offer';
export const WS_RECEIVE_OFFER: string = 'ws://offer';
export const WS_SEND_ANSWER: string = 'ws://answer';
export const WS_RECEIVE_ANSWER: string = 'ws://answer';
export const WS_SEND_ICE_CANDIDATE: string = 'ws://ice';
export const WS_RECEIVE_ICE_CANDIDATE: string = 'ws://ice';
export const WS_SEND_LEAVE: string = 'ws://leave';
export const WS_RECEIVE_LEAVE: string = 'ws://leave';

export const WS_SEND_NEIGHBORS: string = 'ws://neighbors';
export const WS_RECEIVE_NEIGHBORS: string = 'ws://neighbors';

export const WS_SEND_POSITION: string = 'ws://position';
export const WS_RECEIVE_POSITION: string = 'ws://position';
