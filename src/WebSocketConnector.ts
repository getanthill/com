import io from 'socket.io-client';

import { WS_SEND_JOIN } from './constants';

import {
  ConnectorUUID,
  WebSocketConnectorConfig,
  WebSocketConnectorI,
} from '../typings/index';

import Connector from './Connector';

const EVENTS_PREFIX = Object.freeze('ws://');

export default class WebSocketConnector
  extends Connector
  implements WebSocketConnectorI {
  config: WebSocketConnectorConfig = {};
  socket = null;

  constructor(uuid: ConnectorUUID, config: WebSocketConnectorConfig) {
    super(uuid);

    this.config = {
      ...this.config,
      ...config,
    };
  }

  connect() {
    return new Promise((resolve, reject) => {
      // @ts-ignore
      this.socket = io(this.config.url, this.config.options);

      this.socket.on('disconnect', this.onDisconnect.bind(this));
      this.socket.on('connect', () => {
        this.emit(WS_SEND_JOIN);

        resolve(this.socket);
      });
      /**
       * @fixme
       * @see WebSocketConnector.test.ts "raises an exception on a connection error"
       */
      /* istanbul ignore next */
      this.socket.on('connect_error', (err) => reject(err));
    });
  }

  onDisconnect() {
    this.socket = null;
  }

  disconnect() {
    this.socket.disconnect(true);
  }

  emit(event: string | symbol, ...args: any[]): boolean {
    if (event.toString().startsWith(EVENTS_PREFIX)) {
      this.socket.emit(
        event,
        this.superchargeMessage({
          ...args[0],
        }),
      );

      return true;
    }

    return super.emit(event, ...args);
  }

  on(event: string | symbol, listener: (...args: any[]) => void): this {
    if (event.toString().startsWith(EVENTS_PREFIX)) {
      this.socket.on(event, listener);

      return this;
    }

    return super.on(event, listener);
  }
}
