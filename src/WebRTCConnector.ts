import {
  ConnectorUUID,
  WebRTCConnectorConfig,
  WebRTCConnectorI,
} from '../typings/index';

import Connector from './Connector';
import WebRTCNegociator from './WebRTCNegociator';
import WebSocketConnector from './WebSocketConnector';

const EVENTS_PREFIX = Object.freeze('rtc://');

export default class WebRTCConnector
  extends Connector
  implements WebRTCConnectorI {
  config: WebRTCConnectorConfig = {
    serversFetchUrl: '/api/servers.json',
    negociator: {},
  };
  negociator: WebRTCNegociator = null;

  constructor(
    uuid: ConnectorUUID,
    config: WebRTCConnectorConfig,
    ws: WebSocketConnector,
  ) {
    super(uuid);

    this.config = {
      ...this.config,
      ...config,
    };

    this.negociator = new WebRTCNegociator(this.config.negociator, ws);
    this.negociator.on('ready', this.onNegociationReady.bind(this));
  }

  static serialize(message) {
    return JSON.stringify(message);
  }

  static deserialize(message) {
    return JSON.parse(message);
  }

  onNegociationReady({ to, channel }) {
    channel.onmessage = this.onMessage.bind(this);

    super.emit('ready', { to, channel });
  }

  connect(uuid) {
    return this.negociator.connect(uuid);
  }

  onMessage(message) {
    const data = WebRTCConnector.deserialize(message.data);

    super.emit(data.event, data);
  }

  emit(event: string | symbol, ...args: any[]): boolean {
    if (event.toString().startsWith(EVENTS_PREFIX)) {
      const msg = this.superchargeMessage({
        event,
        ...args[0],
      });
      const channel = this.negociator.dataChannels[msg.to];

      channel.send(WebRTCConnector.serialize(msg));

      return true;
    }

    return super.emit(event, ...args);
  }
}
