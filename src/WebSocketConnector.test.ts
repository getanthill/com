import { EventEmitter } from 'events';

import WebSocketConnector from './WebSocketConnector';

function mockSocketIoClient(url, options) {
  const socket = new EventEmitter();

  // @ts-ignore
  socket.disconnect = () => {
    socket.emit('disconnect');
  };

  const onMock = jest.spyOn(socket, 'on');
  onMock.mockImplementation(() => null);

  // @ts-ignore
  socket.url = url;
  // @ts-ignore
  socket.options = options;

  return socket;
}

jest.mock('socket.io-client', () => mockSocketIoClient);

describe('WebSocketConnector', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('constructor', () => {
    it('returns an instance of a WebSocketConnector', () => {
      const connector = new WebSocketConnector('alice', {});
      expect(connector).toBeInstanceOf(WebSocketConnector);
    });

    it('stores the connector `uuid`', () => {
      const connector = new WebSocketConnector('uuid', {});
      expect(connector.uuid).toEqual('uuid');
    });

    it('stores the websocket configurattion', () => {
      const connector = new WebSocketConnector('uuid', {
        url: 'url',
        options: {
          forceNew: true,
        },
      });
      expect(connector.config).toEqual({
        url: 'url',
        options: {
          forceNew: true,
        },
      });
    });
  });

  describe('#connect', () => {
    it('returns a valid socket', () => {
      const connector = new WebSocketConnector('alice', {});

      connector.connect();

      // expect(connector.socket).toBe(socket);
      expect(connector.socket.on).toHaveBeenCalledTimes(3);
      expect(connector.socket.on.mock.calls[0][0]).toEqual('disconnect');
      expect(connector.socket.on.mock.calls[1][0]).toEqual('connect');
      expect(connector.socket.on.mock.calls[2][0]).toEqual('connect_error');
    });

    it('uses the config url to instanciates the websocket connection', () => {
      const connector = new WebSocketConnector('uuid', {
        url: '/my/url',
      });

      connector.connect();

      expect(connector.socket.url).toEqual('/my/url');
    });

    it('uses the config options to instanciates the websocket connection', () => {
      const connector = new WebSocketConnector('uuid', {
        options: {
          forceNew: true,
        },
      });

      connector.connect();

      expect(connector.socket.options).toEqual({
        forceNew: true,
      });
    });

    it.skip('raises an exception on a connection error', async () => {
      /**
       * I don't find a clean way to test this part... Jest mocks
       * do not seem to solve the issue. I would like to restore and
       * apply another mock with the spy on the `on` method. But
       * this does not
       */
      const connector = new WebSocketConnector('alice', {});
      let error;

      jest.restoreAllMocks();

      try {
        const promise = connector.connect();
        connector.socket.emit('connect_error', new Error('Ooops'));

        await promise;
      } catch (err) {
        error = err;
      }

      console.log(error);
      expect(error).toBeInstanceOf(Error);
    });
  });

  describe('#disconnect', () => {
    it('closes the websocket connection', () => {
      const connector = new WebSocketConnector('alice', {});

      expect(connector.socket).toEqual(null);

      connector.connect();

      connector.socket.on.mockRestore();
      connector.socket.on('disconnect', connector.onDisconnect.bind(connector));

      connector.disconnect();

      expect(connector.socket).toEqual(null);
    });
  });

  describe('#emit', () => {
    let connector;
    const now = new Date('2020-10-02').getTime();

    beforeEach(() => {
      connector = new WebSocketConnector('uuid', {});
      connector.now = jest.fn().mockImplementation(() => now);

      connector.connect();
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('emits events only starting with `EVENTS_PREFIX`', () => {
      connector.socket.on.mockRestore();

      let event;

      connector.socket.on('ws://hello', (evt) => (event = evt));

      connector.emit('ws://hello', { world: true });

      expect(event).toEqual({
        ts: now,
        from: 'uuid',
        world: true,
      });
    });

    it('emits non prefixed events over the EventEmitter', () => {
      let event;

      connector.on('XX_hello', (evt) => (event = evt));

      connector.emit('XX_hello', { world: true });

      expect(event).toEqual({
        ts: now,
        from: 'uuid',
        world: true,
      });
    });
  });

  describe('#on', () => {
    it('binds event on the EventEmitter', () => {
      const connector = new WebSocketConnector('alice', {});

      let event;
      connector.on('hello', (evt) => (event = evt));

      connector.emit('hello', { world: true });

      expect(event).toMatchObject({
        world: true,
      });
    });

    it('binds event on the websocket connection if the event starts with `EVENTS_PREFIX`', () => {
      const connector = new WebSocketConnector('alice', {});
      connector.connect();

      connector.socket.on.mockRestore();

      let event;
      connector.on('ws://hello', (evt) => (event = evt));

      connector.socket.emit('ws://hello', { world: true });

      expect(event).toMatchObject({
        world: true,
      });
    });
  });
});
