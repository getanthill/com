import { EventEmitter } from 'events';

import WebSocketConnector from './WebSocketConnector';
import WebRTCNegociator from './WebRTCNegociator';

describe('WebRTCNegociator', () => {
  const UUID = 'uuid';

  interface FakeDataChannel {
    onmessage?: Function;
    onopen?: Function;
  }

  interface FakeRTCPeerConnection {
    signalingState?: String;
    createOffer?: Function;
    createAnswer?: Function;
    createDataChannel?: Function;
    setLocalDescription?: Function;
    setRemoteDescription?: Function;
    addIceCandidate?: Function;
    close?: Function;
  }

  const ICE_SERVERS = [
    {
      url: 'stun:server1:3478',
      urls: 'turn:server1:3478',
    },
    {
      username: 'username',
      credential: 'credential',
      url: 'turn:server1:3478',
      urls: 'turn:server1:3478',
    },
  ];

  beforeEach(() => {
    // ws = new WebSocketConnector(UUID, {});
    // ws.connect();

    window.fetch = jest.fn().mockResolvedValue({
      json: async () => ({
        iceServers: ICE_SERVERS,
        ttl: 86400,
      }),
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('constructor', () => {
    let ws: WebSocketConnector;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();

      ws.connect();
    });

    it('returns an instance of a WebRTCNegociator', () => {
      const negociator = new WebRTCNegociator({}, ws);
      expect(negociator).toBeInstanceOf(WebRTCNegociator);
    });

    it('uses default configuration', () => {
      const negociator = new WebRTCNegociator({}, ws);
      expect(negociator.config).toEqual({
        iceServers: [],
        iceServersFetchUrl: '/api/servers.json',
        iceServersSyncTTLThreshold: 0.9,
      });
    });

    it('stores the websocket connection', () => {
      const negociator = new WebRTCNegociator({}, ws);
      expect(negociator.ws).toBe(ws);
    });
  });

  describe('#fetchServers', () => {
    let ws: WebSocketConnector;
    let negociator: WebRTCNegociator;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();

      negociator = new WebRTCNegociator({}, ws);
    });

    it('fetches ICE servers from backend', async () => {
      expect(await negociator.fetchIceServers()).toEqual({
        iceServers: ICE_SERVERS,
        ttl: 86400,
      });
    });
  });

  describe('#updateServers', () => {
    let ws: WebSocketConnector;
    let negociator: WebRTCNegociator;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();

      negociator = new WebRTCNegociator({}, ws);
    });

    it('updates connector ICE servers list from anthill backend', async () => {
      await negociator.updateIceServers();

      expect(negociator.config.iceServers).toEqual(ICE_SERVERS);
    });
  });

  describe('#startIceServersSynchronization', () => {
    let ws: WebSocketConnector;
    let negociator: WebRTCNegociator;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();

      negociator = new WebRTCNegociator({}, ws);
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('starts a loop to refresh ICE servers before they expired', async () => {
      (global as any).setTimeout = jest.fn().mockImplementation(() => 123456);

      await negociator.startIceServersSynchronization();

      expect(negociator.iceServersSetTimeout).toEqual(123456);
      expect((global as any).setTimeout).toHaveBeenCalledTimes(1);
      expect((global as any).setTimeout.mock.calls[0][1]).toEqual(77760000);
    });

    it('clears previously set timeout if calls before the next call', async () => {
      (global as any).clearTimeout = jest.fn().mockImplementation(() => null);
      (global as any).setTimeout = jest
        .fn()
        .mockImplementationOnce(() => 654321)
        .mockImplementation(() => 123456);

      await negociator.startIceServersSynchronization();
      await negociator.startIceServersSynchronization();

      expect(negociator.iceServersSetTimeout).toEqual(123456);
      expect((global as any).clearTimeout).toHaveBeenCalledTimes(1);
      expect((global as any).setTimeout).toHaveBeenCalledTimes(2);
    });
  });

  describe('#createRTC', () => {
    let ws: WebSocketConnector;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();

      // Mock RTCPeerConnection
      (global as any).RTCPeerConnection = jest.fn();
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('must create a new WebRTC connection with another user', () => {
      const joe = new WebRTCNegociator({}, ws);

      ws.emit = jest.fn();
      const onConnectionReady = jest.fn();
      joe.on('ready', onConnectionReady);

      joe.createRTC('jack');

      expect(RTCPeerConnection).toHaveBeenCalledTimes(1);
      expect(RTCPeerConnection).toHaveBeenCalledWith({
        iceServers: [],
      });

      expect(joe.peerConnections).toHaveProperty('jack');

      // Checks types and bindings:
      const jackPeerConnection = joe.peerConnections['jack'];
      expect(jackPeerConnection).toBeInstanceOf(RTCPeerConnection);
      expect(typeof jackPeerConnection.onicecandidate).toBe('function');
      expect(typeof jackPeerConnection.ondatachannel).toBe('function');

      expect(joe.makingOffer.get('jack')).toBe(false);

      // Checks on ice candidate receive trigger:
      jackPeerConnection.onicecandidate({}); // With no candidate
      expect(ws.emit).toHaveBeenCalledTimes(0);

      jackPeerConnection.onicecandidate({ candidate: 'jack_candidate' }); // with candidate
      expect(ws.emit).toHaveBeenCalledTimes(1);
      expect(ws.emit).toHaveBeenCalledWith('ws://ice', {
        candidate: 'jack_candidate',
        to: 'jack',
      });

      // Checks on data channel creation trigger:
      const fakeDataChannel: FakeDataChannel = {};
      jackPeerConnection.ondatachannel({ channel: fakeDataChannel });
      expect(joe.dataChannels).toHaveProperty('jack');
      expect(joe.dataChannels['jack']).toEqual(fakeDataChannel);

      // expect(typeof fakeDataChannel.onmessage).toBe('function');
      expect(onConnectionReady).toHaveBeenCalledTimes(1);
      expect(onConnectionReady).toHaveBeenCalledWith({
        to: 'jack',
        channel: fakeDataChannel,
      });

      expect(joe.makingOffer.get('jack')).toBe(undefined);
    });
  });

  describe('#initiateSignaling', () => {
    let ws: WebSocketConnector;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('must initiate the signaling with another user', async () => {
      const peerConnection: FakeRTCPeerConnection = {
        createOffer: jest.fn().mockImplementation(() => ({
          offer: 'offer',
        })),
        setLocalDescription: jest.fn(),
        signalingState: 'stable',
      };

      const joe = new WebRTCNegociator({}, ws);

      ws.emit = jest.fn();
      joe.initiateDataChannel = jest.fn();

      await joe.initiateSignaling(peerConnection, 'jack');

      expect(joe.makingOffer.get('jack')).toEqual(false);
      expect(peerConnection.createOffer).toHaveBeenCalledTimes(1);
      expect(peerConnection.createOffer).toHaveBeenCalledWith({
        offerToReceiveVideo: false,
        offerToReceiveAudio: false,
      });

      expect(peerConnection.setLocalDescription).toHaveBeenCalledTimes(1);
      expect(peerConnection.setLocalDescription).toHaveBeenCalledWith({
        offer: 'offer',
      });

      expect(ws.emit).toHaveBeenCalledTimes(1);
      expect(ws.emit).toHaveBeenCalledWith('ws://offer', {
        offer: { offer: 'offer' },
        to: 'jack',
      });
    });

    it('must keep the making offer true if the peerConnection is not stable', async () => {
      const peerConnection: FakeRTCPeerConnection = {
        createOffer: jest.fn().mockImplementation(() => ({
          offer: 'offer',
        })),
        setLocalDescription: jest.fn(),
        signalingState: 'have-local-offer', // not stable
      };

      const joe = new WebRTCNegociator({}, ws);

      ws.emit = jest.fn();
      joe.initiateDataChannel = jest.fn();

      await joe.initiateSignaling(peerConnection, 'jack');

      expect(joe.makingOffer.get('jack')).toEqual(false);
      expect(peerConnection.setLocalDescription).toHaveBeenCalledTimes(0);
      expect(ws.emit).toHaveBeenCalledTimes(0);
    });

    it('must throw an exception but set makingOffer to false', async () => {
      // Avoid error message printed to the console
      jest.spyOn(global.console, 'error').mockImplementation(() => jest.fn());

      let error;

      const peerConnection: FakeRTCPeerConnection = {
        createOffer: jest.fn().mockImplementation(() => {
          throw new Error('Ooops');
        }),
        setLocalDescription: jest.fn(),
        signalingState: 'have-local-offer', // not stable
      };

      const joe = new WebRTCNegociator({}, ws);

      ws.emit = jest.fn();
      joe.initiateDataChannel = jest.fn();

      try {
        await joe.initiateSignaling(peerConnection, 'jack');
      } catch (err) {
        error = err;
      }

      expect(error).toEqual(new Error('Ooops'));
      expect(joe.makingOffer.get('jack')).toEqual(false);
    });
  });

  describe('#initiateDataChannel', () => {
    let ws: WebSocketConnector;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('must initiate the Data Channel', () => {
      const dataChannel: FakeDataChannel = {};
      const peerConnection: FakeRTCPeerConnection = {
        createOffer: jest.fn(),
        createDataChannel: jest.fn().mockImplementation(() => dataChannel),
        setLocalDescription: jest.fn(),
        signalingState: 'have-local-offer', // not stable
      };

      const joe = new WebRTCNegociator({}, ws);

      joe.emit = jest.fn();

      joe.initiateDataChannel(peerConnection, 'jack');

      expect(joe.dataChannels).toHaveProperty('jack');
      expect(joe.dataChannels['jack']).toBe(dataChannel);

      expect(typeof dataChannel.onopen).toEqual('function');

      dataChannel.onopen();

      // expect(typeof dataChannel.onmessage).toEqual('function');
      expect(joe.emit).toHaveBeenCalledTimes(1);
      expect(joe.emit).toHaveBeenCalledWith('ready', {
        to: 'jack',
        channel: dataChannel,
      });

      expect(joe.makingOffer.get('jack')).toEqual(undefined);
    });
  });

  describe('#onReceiveOfferOrAnswer', () => {
    let peerConnection: FakeRTCPeerConnection;
    let joe;
    let ws: WebSocketConnector;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();

      peerConnection = {
        createOffer: jest.fn(),
        createAnswer: jest.fn().mockImplementation(() => ({ type: 'answer' })),
        setLocalDescription: jest.fn(),
        setRemoteDescription: jest.fn(),
        signalingState: 'stable',
      };

      // Mock RTCPeerConnection
      (global as any).RTCPeerConnection = jest
        .fn()
        .mockImplementation(() => peerConnection);

      joe = new WebRTCNegociator({}, ws);
      ws.emit = jest.fn();
      joe.emit = jest.fn();
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('must send an answer if an offer is received', async () => {
      const handler = joe.onReceiveOfferOrAnswer();

      await handler({ description: { type: 'offer' }, from: 'jack' });

      expect(joe.peerConnections).toHaveProperty('jack');
      expect(peerConnection.setRemoteDescription).toHaveBeenCalledTimes(1);
      expect(peerConnection.setRemoteDescription).toHaveBeenCalledWith({
        type: 'offer',
      });

      expect(peerConnection.createAnswer).toHaveBeenCalledTimes(1);

      expect(peerConnection.setLocalDescription).toHaveBeenCalledTimes(1);
      expect(peerConnection.setLocalDescription).toHaveBeenCalledWith({
        type: 'answer',
      });

      expect(ws.emit).toHaveBeenCalledTimes(1);
      expect(ws.emit).toHaveBeenCalledWith('ws://answer', {
        answer: { type: 'answer' },
        to: 'jack',
      });
    });

    it('must accept an answer if an offer has been sent', async () => {
      joe.createRTC('jack');

      const handler = joe.onReceiveOfferOrAnswer();

      await handler({ description: { type: 'answer' }, from: 'jack' });

      expect(joe.peerConnections).toHaveProperty('jack');
      expect(peerConnection.setRemoteDescription).toHaveBeenCalledTimes(1);
      expect(peerConnection.setRemoteDescription).toHaveBeenCalledWith({
        type: 'answer',
      });

      expect(peerConnection.createAnswer).toHaveBeenCalledTimes(0);
    });
  });

  describe('#onReceiveIceCandidate', () => {
    let peerConnection: FakeRTCPeerConnection;
    let joe;
    let ws: WebSocketConnector;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();

      peerConnection = {
        addIceCandidate: jest.fn(),
      };

      joe = new WebRTCNegociator({}, ws);
      joe.peerConnections['jack'] = peerConnection;
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('invokes the `addIceCandidate` of the correct peerConnection', () => {
      joe.onReceiveIceCandidate()({
        candidate: { candidate: 1 },
        from: 'jack',
      });

      expect(joe.peerConnections['jack'].addIceCandidate).toHaveBeenCalledTimes(
        1,
      );
      expect(joe.peerConnections['jack'].addIceCandidate).toHaveBeenCalledWith({
        candidate: 1,
      });
    });
  });

  describe('#onReceiveClientLeave', () => {
    let joe;
    let ws: WebSocketConnector;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();

      joe = new WebRTCNegociator({}, ws);
      joe.peerConnections['jack'] = 'peerConnection';
      joe.dataChannels['jack'] = 'dataChannel';
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('invokes the `addIceCandidate` of the correct peerConnection', () => {
      joe.onReceiveClientLeave()('jack');

      expect(joe.peerConnections['jack']).toBeUndefined();
      expect(joe.dataChannels['jack']).toBeUndefined();
    });
  });

  describe('#disconnect', () => {
    let ws: WebSocketConnector;

    beforeEach(() => {
      ws = new WebSocketConnector(UUID, {});
      ws.connect();
    });

    it('removes peerConnection and dataChannel from dictionnaries', () => {
      const dataChannel: FakeDataChannel = {};
      const peerConnection: FakeRTCPeerConnection = {
        close: jest.fn(),
      };

      const alice = new WebRTCNegociator({}, ws);

      alice.peerConnections['bernard'] = peerConnection;
      alice.dataChannels['bernard'] = dataChannel;

      alice.disconnect('bernard');

      expect(alice.peerConnections['bernard']).toBeUndefined();
      expect(alice.dataChannels['bernard']).toBeUndefined();
    });

    it('closes the peerConnection if available', () => {
      const dataChannel: FakeDataChannel = {};
      const peerConnection: FakeRTCPeerConnection = {
        close: jest.fn(),
      };

      const alice = new WebRTCNegociator({}, ws);

      alice.peerConnections['bernard'] = peerConnection;
      alice.dataChannels['bernard'] = dataChannel;

      alice.disconnect('bernard');

      expect(peerConnection.close).toHaveBeenCalledTimes(1);
    });

    it('does nothing if already closed or non-existent', () => {
      const alice = new WebRTCNegociator({}, ws);

      alice.disconnect('bernard');
    });
  });
});
