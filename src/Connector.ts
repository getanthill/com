import { EventEmitter } from 'events';
import { ConnectorUUID, ConnectorI, Message } from '../typings';

export default class Connector extends EventEmitter implements ConnectorI {
  public uuid: ConnectorUUID = null;

  constructor(uuid: ConnectorUUID) {
    super();

    this.uuid = uuid;
  }

  now(): number {
    return Date.now();
  }

  superchargeMessage(message: object = {}): Message {
    return {
      from: this.uuid,
      ts: this.now(),
      ...message,
    };
  }

  emit(event: string | symbol, ...args: any[]): boolean {
    return super.emit(event, ...args.map(this.superchargeMessage.bind(this)));
  }

  broadcast(event: string | symbol, ...args: any[]): boolean {
    return this.emit(event, ...args);
  }
}
