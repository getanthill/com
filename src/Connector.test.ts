import Connector from './Connector';

describe('Connector', () => {
  describe('constructor', () => {
    it('returns an instance of a Connector', () => {
      const connector = new Connector('alice');
      expect(connector).toBeInstanceOf(Connector);
    });

    it('returns an instance of a Connector with the correct `uuid`', () => {
      const connector = new Connector('uuid');
      expect(connector.uuid).toEqual('uuid');
    });
  });

  describe('#now', () => {
    const connector = new Connector('alice');

    it('returns a number reflecting Date.now', () => {
      const now = connector.now();
      expect(now).toBeGreaterThan(0);
      expect(now).toBeLessThanOrEqual(Date.now());
    });
  });

  describe('#superchargeMessage', () => {
    let connector: Connector;
    const now = new Date('2020-10-02').getTime();

    beforeEach(() => {
      connector = new Connector('alice');
      connector.now = jest.fn().mockImplementation(() => now);
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('supercharges a message with the `uuid` of the connector', () => {
      connector.uuid = 'uuid';
      const msg = connector.superchargeMessage({
        hello: 'world',
      });

      expect(msg).toHaveProperty('from', 'uuid');
    });

    it('supercharges a message with the timestamp `ts` value', () => {
      const msg = connector.superchargeMessage({
        hello: 'world',
      });

      expect(msg).toHaveProperty('ts', now);
    });

    it('supercharges an empty message (default = {})', () => {
      const msg = connector.superchargeMessage();

      expect(msg).toHaveProperty('from');
      expect(msg).toHaveProperty('ts');
    });
  });

  describe('#emit', () => {
    let connector: Connector;
    const now = new Date('2020-10-02').getTime();

    beforeEach(() => {
      connector = new Connector('uuid');
      connector.now = jest.fn().mockImplementation(() => now);
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('emits an event over the EventEmitter with a supercharged message', () => {
      let event = null;

      connector.on('hello', (ev) => {
        event = ev;
      });

      connector.emit('hello', { world: true });

      expect(event).toEqual({
        ts: now,
        from: 'uuid',
        world: true,
      });
    });
  });

  describe('#broadcast', () => {
    let connector: Connector;

    beforeEach(() => {
      connector = new Connector('alice');
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('proxies the `emit` method', () => {
      connector.emit = jest.fn();

      connector.broadcast('hello', { world: true });

      expect(connector.emit).toHaveBeenCalledTimes(1);
      expect(connector.emit).toHaveBeenCalledWith('hello', {
        world: true,
      });
    });
  });
});
