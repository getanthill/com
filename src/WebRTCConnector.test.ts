import { EventEmitter } from 'events';

import WebRTCConnector from './WebRTCConnector';
import WebSocketConnector from './WebSocketConnector';

function mockSocketIoClient(url, options) {
  const socket = new EventEmitter();

  // @ts-ignore
  socket.disconnect = () => {
    socket.emit('disconnect');
  };

  const onMock = jest.spyOn(socket, 'on');
  onMock.mockImplementation(() => null);

  // @ts-ignore
  socket.url = url;
  // @ts-ignore
  socket.options = options;

  return socket;
}

jest.mock('socket.io-client', () => mockSocketIoClient);

describe('WebRTCConnector', () => {
  let ws;

  interface FakeDataChannel {
    onmessage?: Function;
    onopen?: Function;
    send?: Function;
  }

  beforeEach(() => {
    ws = new WebSocketConnector('uuid', {});
    ws.connect();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('static #serialize', () => {
    it('serializes an object into a JSON string', () => {
      expect(WebRTCConnector.serialize({ hello: 'world' })).toEqual(
        '{"hello":"world"}',
      );
    });
  });

  describe('static #deserialize', () => {
    it('deserializes a JSON string into an object', () => {
      expect(WebRTCConnector.deserialize('{"hello":"world"}')).toEqual({
        hello: 'world',
      });
    });
  });

  describe('constructor', () => {
    it('returns an instance of a WebRTCConnector', () => {
      const connector = new WebRTCConnector('alice', {}, ws);
      expect(connector).toBeInstanceOf(WebRTCConnector);
    });

    it('stores the connector `uuid`', () => {
      const connector = new WebRTCConnector('alice', {}, ws);
      expect(connector.uuid).toEqual('alice');
    });

    it('stores the websocket configurattion', () => {
      const connector = new WebRTCConnector(
        'alice',
        {
          serversFetchUrl: '/api/servers',
        },
        ws,
      );
      expect(connector.config).toEqual({
        negociator: {},
        serversFetchUrl: '/api/servers',
      });
    });
  });

  describe('#connect', () => {
    it('starts the negociation process', () => {
      const connector = new WebRTCConnector('alice', {}, ws);
      connector.negociator.connect = jest.fn();

      connector.connect('bernard');

      expect(connector.negociator.connect).toHaveBeenCalledTimes(1);
      expect(connector.negociator.connect).toHaveBeenCalledWith('bernard');
    });
  });

  describe('#onNegociationReady', () => {
    it('binds onMessage on the DataChannel.onmessage handler', () => {
      const channel: FakeDataChannel = {};
      const connector = new WebRTCConnector('alice', {}, ws);

      let onReadyEvent;
      connector.on('ready', (evt) => (onReadyEvent = evt));

      connector.negociator.emit('ready', { to: 'bernard', channel });

      expect(typeof channel.onmessage).toEqual('function');
      expect(onReadyEvent).toMatchObject({
        to: 'bernard',
        channel,
      });
    });
  });

  describe('#onMessage', () => {
    it('emits any message received over WebRTC', () => {
      const channel: FakeDataChannel = {};
      const connector = new WebRTCConnector('alice', {}, ws);

      let message;
      connector.on('rtc://hello', (msg) => (message = msg));

      connector.negociator.connect = jest.fn();

      connector.connect('bernard'); // useless here, just for clarity
      connector.negociator.emit('ready', { to: 'bernard', channel });

      channel.onmessage({ data: '{"event":"rtc://hello","world":true}' });
      expect(message).toMatchObject({
        event: 'rtc://hello',
        world: true,
      });
    });
  });

  describe('#emit', () => {
    it('emits a message on an already available dataChannel', () => {
      const connector = new WebRTCConnector('alice', {}, ws);

      const now = new Date('2020-10-02').getTime();
      connector.now = jest.fn().mockImplementation(() => now);

      const channel: FakeDataChannel = {
        send: jest.fn(),
      };

      connector.negociator.connect = jest.fn();
      connector.connect('bernard'); // useless here, just for clarity
      connector.negociator.dataChannels['bernard'] = channel;

      connector.negociator.emit('ready', { to: 'bernard', channel });

      connector.emit('rtc://hello', { to: 'bernard', world: true });

      expect(channel.send).toHaveBeenCalledTimes(1);
      expect(channel.send).toHaveBeenCalledWith(
        '{"from":"alice","ts":1601596800000,"event":"rtc://hello","to":"bernard","world":true}',
      );
    });

    it('emits a message with the EventEmitter if the prefix is not found', () => {
      const connector = new WebRTCConnector('alice', {}, ws);

      const now = new Date('2020-10-02').getTime();
      connector.now = jest.fn().mockImplementation(() => now);

      let message;
      connector.on('hello', (msg) => (message = msg));

      const channel: FakeDataChannel = {
        send: jest.fn(),
      };

      connector.negociator.connect = jest.fn();
      connector.connect('bernard'); // useless here, just for clarity
      connector.negociator.dataChannels['bernard'] = channel;

      connector.negociator.emit('ready', { to: 'bernard', channel });

      connector.emit('hello', { to: 'bernard', world: true });

      expect(channel.send).toHaveBeenCalledTimes(0);
      expect(message).toEqual({
        from: 'alice',
        to: 'bernard',
        ts: 1601596800000,
        world: true,
      });
    });
  });
});
