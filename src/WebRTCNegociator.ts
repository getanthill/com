import { EventEmitter } from 'events';
import { WebRTCNegociatorConfig, WebRTCNegociatorI } from '../typings';
import WebSocketConnector from './WebSocketConnector';

import * as c from './constants';

export default class WebRTCNegociator
  extends EventEmitter
  implements WebRTCNegociatorI {
  config: WebRTCNegociatorConfig = {
    iceServers: [],
    iceServersFetchUrl: '/api/servers.json',
    iceServersSyncTTLThreshold: 0.9,
  };

  ws: WebSocketConnector;

  iceServersSetTimeout: NodeJS.Timeout;

  private RTCConfiguration: RTCConfiguration = {};
  dataChannels = new Map<string, RTCDataChannel>();
  peerConnections = new Map<string, RTCPeerConnection>();

  private offerOptions: RTCOfferOptions = {
    /**
     * @warning Deprecation
     * @see https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/createOffer#RTCOfferOptions_dictionary
     * @see https://developer.mozilla.org/en-US/docs/Web/API/RTCRtpTransceiver
     */
    offerToReceiveVideo: false,
    offerToReceiveAudio: false,
  };

  makingOffer = new Map<string, boolean>();

  constructor(config: WebRTCNegociatorConfig, ws: WebSocketConnector) {
    super();

    this.config = {
      ...this.config,
      ...config,
    };

    this.ws = ws;

    this.ws.on(c.WS_RECEIVE_OFFER, this.onReceiveOfferOrAnswer());
    this.ws.on(c.WS_RECEIVE_ANSWER, this.onReceiveOfferOrAnswer());
    this.ws.on(c.WS_RECEIVE_ICE_CANDIDATE, this.onReceiveIceCandidate());
    this.ws.on(c.WS_RECEIVE_LEAVE, this.onReceiveClientLeave());
  }

  async fetchIceServers() {
    const res = await fetch('/api/servers.json');

    return res.json();
  }

  async updateIceServers() {
    const res = await this.fetchIceServers();
    this.config.iceServers = res.iceServers;

    return res.ttl;
  }

  async startIceServersSynchronization() {
    if (this.iceServersSetTimeout) {
      clearTimeout(this.iceServersSetTimeout);
    }

    const ttl = await this.updateIceServers();

    this.iceServersSetTimeout = setTimeout(
      this.startIceServersSynchronization.bind(this),
      ttl * this.config.iceServersSyncTTLThreshold * 1000, // 90% of the TTL
    );
  }

  createRTC(to) {
    this.makingOffer.set(to, false);

    let peerConnection = new RTCPeerConnection({
      ...this.RTCConfiguration,
      iceServers: this.config.iceServers,
    });

    peerConnection.onicecandidate = ({ candidate }) => {
      if (candidate) {
        // console.log('3. >> send ice candidate to ', to);
        // console.debug(candidate);
        this.ws.emit(c.WS_SEND_ICE_CANDIDATE, {
          candidate,
          to,
        });
      }
    };

    peerConnection.ondatachannel = ({ channel }) => {
      let dataChannel = channel;

      this.dataChannels[to] = dataChannel;

      /**
       * @fixme emit on event to inform the client of the new channel
       */
      // dataChannel.onmessage = this.receive.bind(this);

      this.emit('ready', { to, channel });

      this.makingOffer.delete(to);
    };

    this.peerConnections[to] = peerConnection;

    return peerConnection;
  }

  async initiateSignaling(peerConnection, to) {
    // console.log('1.b Initiate Signaling');
    this.initiateDataChannel(peerConnection, to);

    try {
      this.makingOffer.set(to, true);
      const offer = await peerConnection.createOffer(this.offerOptions);

      if (peerConnection.signalingState !== 'stable') return;

      peerConnection.setLocalDescription(offer);

      // console.log('2. >> send offer to ' + to);
      // console.debug(offer);

      this.ws.emit(c.WS_SEND_OFFER, { offer, to });
    } catch (err) {
      console.error(err);

      throw err;
    } finally {
      this.makingOffer.set(to, false);
    }
  }

  initiateDataChannel(peerConnection, to) {
    // console.log('1.c Create Data Channel');
    const dataChannel = peerConnection.createDataChannel('messageChannel', {
      negociated: true,
      reliable: false,
      id: 0,
    });

    dataChannel.onopen = () => {
      // console.log('5. <<>> Data Channel opened');

      // dataChannel.onmessage = this.receive.bind(this);

      this.emit('ready', { to, channel: dataChannel });

      this.makingOffer.delete(to);
    };

    this.dataChannels[to] = dataChannel;
  }

  /**
   * @see https://blog.mozilla.org/webrtc/perfect-negotiation-in-webrtc/
   */
  onReceiveOfferOrAnswer() {
    return async ({ description, from }) => {
      let peerConnection = this.peerConnections[from];

      if (!peerConnection) {
        peerConnection = this.createRTC(from);
      }

      const offerCollision =
        description.type === 'offer' &&
        (this.makingOffer.get(from) ||
          peerConnection.signalingState !== 'stable');

      await Promise.all([
        peerConnection.setRemoteDescription(description),
        // Rollback previous local description
        offerCollision
          ? peerConnection.setLocalDescription({ type: 'rollback' })
          : Promise.resolve(),
      ]);

      if (description.type === 'offer') {
        const answer = await peerConnection.createAnswer();
        await peerConnection.setLocalDescription(answer);
        this.ws.emit(c.WS_SEND_ANSWER, { answer, to: from });
      }
    };
  }

  onReceiveIceCandidate() {
    return ({ candidate, from }) => {
      // console.log('5. << receive ice candidate from ' + from);
      // console.debug(candidate);
      this.peerConnections[from].addIceCandidate(candidate);
    };
  }

  onReceiveClientLeave() {
    return (uuid) => {
      // console.log('client leave (' + uuid + ')');
      delete this.peerConnections[uuid];
      delete this.dataChannels[uuid];
    };
  }

  connect(to): Promise<RTCDataChannel> {
    // console.log('1 << connect to client (' + to + ')');
    let peerConnection = this.peerConnections[to];

    const waitReadyState = (): Promise<RTCDataChannel> => {
      return new Promise((resolve) => {
        this.on('ready', ({ to: readyTo, channel }) => {
          /* istanbul ignore next */
          if (to === readyTo) {
            return resolve(channel);
          }
        });
      });
    };

    if (peerConnection) {
      return waitReadyState();
    }

    peerConnection = this.createRTC(to);

    this.initiateSignaling(peerConnection, to);

    return waitReadyState();
  }

  async disconnect(to) {
    // console.log('x >> disconnecting (' + to + ')');
    let peerConnection = this.peerConnections[to];

    if (peerConnection) {
      peerConnection.close();
    }

    delete this.peerConnections[to];
    delete this.dataChannels[to];
  }
}
