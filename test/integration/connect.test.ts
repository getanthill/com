import WebSocketConnector from '../../src/WebSocketConnector';
import WebRTCConnector from '../../src/WebRTCConnector';

import Server from './server';

import * as c from '../../src/constants';

describe('com', () => {
  let server;
  let wsOptions;

  beforeEach(async () => {
    server = new Server(3000);

    await server.connect();

    wsOptions = {
      url: `http://[${server.addr.address}]:${server.addr.port}`,
      options: {
        'reconnection delay': 0,
        'reopen delay': 0,
        'force new connection': true,
        transports: ['websocket'],
      },
    };
  });

  afterEach(async () => {
    await server.disconnect();
  });

  describe('com/websocket', () => {
    describe('communication between 2 clients and the server is possible', () => {
      it('connects to the server', async (done) => {
        const joe = new WebSocketConnector('joe', wsOptions);

        const jack = new WebSocketConnector('jack', wsOptions);

        await Promise.all([joe.connect(), jack.connect()]);

        jack.on(c.WS_RECEIVE_OFFER, (msg) => {
          done();
        });

        joe.emit(c.WS_SEND_OFFER, {
          offer: 'offer',
          to: 'jack',
        });
      });
    });
  });

  describe('com/webrtc', () => {
    describe('negociation between 2 clients is possible', () => {
      interface FakeDataChannel {
        onmessage?: Function;
        onopen?: Function;
        send?: Function;
      }

      interface FakeRTCPeerConnection {
        signalingState?: String;
        createOffer?: Function;
        createAnswer?: Function;
        createDataChannel?: Function;
        setLocalDescription?: Function;
        setRemoteDescription?: Function;
        addIceCandidate?: Function;
        ondatachannel?: Function;
      }

      interface Client {
        ws: WebSocketConnector;
        webrtc: WebRTCConnector;
        dataChannel: FakeDataChannel;
        peerConnection: FakeRTCPeerConnection;
      }

      let alice: Client = {
        ws: null,
        webrtc: null,
        dataChannel: null,
        peerConnection: null,
      };
      let bernard: Client = {
        ws: null,
        webrtc: null,
        dataChannel: null,
        peerConnection: null,
      };

      beforeEach(async () => {
        alice.dataChannel = {
          onopen: jest.fn(),
          send: (str) => {
            return bernard.dataChannel.onmessage({
              data: str,
            });
          },
        };
        bernard.dataChannel = {
          onopen: jest.fn(),
          send: (str) => {
            return alice.dataChannel.onmessage({
              data: str,
            });
          },
        };
        alice.peerConnection = {
          createOffer: jest.fn().mockImplementation(() => ({
            type: 'offer',
          })),
          createAnswer: jest
            .fn()
            .mockImplementation(() => ({ type: 'answer' })),
          createDataChannel: jest
            .fn()
            .mockImplementation(() => alice.dataChannel),
          setLocalDescription: jest.fn().mockImplementation((description) => {
            if (description.type === 'rollback') {
              console.log(121, 'alice');
              alice.peerConnection.signalingState = 'stable';
            }
          }),
          setRemoteDescription: jest.fn().mockImplementation((description) => {
            if (description.type === 'answer') {
              bernard.peerConnection.ondatachannel({
                channel: bernard.dataChannel,
              });

              alice.peerConnection.ondatachannel({
                channel: alice.dataChannel,
              });
            }
          }),
          signalingState: 'stable',
        };
        bernard.peerConnection = {
          createOffer: jest.fn().mockImplementation(() => ({
            type: 'offer',
          })),
          createAnswer: jest
            .fn()
            .mockImplementation(() => ({ type: 'answer' })),
          createDataChannel: jest
            .fn()
            .mockImplementation(() => bernard.dataChannel),
          setLocalDescription: jest.fn().mockImplementation((description) => {
            if (description.type === 'answer') {
              bernard.peerConnection.ondatachannel({
                channel: bernard.dataChannel,
              });
            }
          }),
          setRemoteDescription: jest.fn(),
          signalingState: 'stable',
        };

        // Mock RTCPeerConnection
        (global as any).RTCPeerConnection = jest
          .fn()
          .mockImplementationOnce(() => alice.peerConnection)
          .mockImplementation(() => bernard.peerConnection);

        alice.ws = new WebSocketConnector('alice', wsOptions);
        bernard.ws = new WebSocketConnector('bernard', wsOptions);

        await Promise.all([alice.ws.connect(), bernard.ws.connect()]);

        alice.webrtc = new WebRTCConnector('alice', {}, alice.ws);
        bernard.webrtc = new WebRTCConnector('bernard', {}, bernard.ws);
      });

      afterEach(() => {
        jest.resetAllMocks();
      });

      it('negociates WebRTC over WebSocket', async (done) => {
        bernard.webrtc.on('rtc://hello', (data) => {
          expect(data).toMatchObject({
            from: 'alice',
            to: 'bernard',
            world: true,
          });

          bernard.webrtc.emit('rtc://hello', {
            to: data.from,
            world: true,
          });
        });

        alice.webrtc.on('rtc://hello', (data) => {
          expect(data).toMatchObject({
            from: 'bernard',
            to: 'alice',
            world: true,
          });
          done();
        });

        await alice.webrtc.connect('bernard');

        alice.webrtc.emit('rtc://hello', {
          to: 'bernard',
          world: true,
        });
      });

      it('manages concurrent negociation properly', async (done) => {
        bernard.webrtc.on('rtc://hello', (data) => {
          expect(data).toMatchObject({
            from: 'alice',
            to: 'bernard',
            world: true,
          });

          bernard.webrtc.emit('rtc://hello', {
            to: data.from,
            world: true,
          });
        });

        alice.webrtc.on('rtc://hello', (data) => {
          expect(data).toMatchObject({
            from: 'bernard',
            to: 'alice',
            world: true,
          });
          done();
        });

        // alice.peerConnection.signalingState = 'have-local-offer';
        bernard.peerConnection.signalingState = 'have-local-offer';

        await alice.webrtc.connect('bernard');

        alice.webrtc.emit('rtc://hello', {
          to: 'bernard',
          world: true,
        });
      });

      it('manages multiple connection attempts correctly', async () => {
        const channels = await Promise.all([
          alice.webrtc.connect('bernard'),
          alice.webrtc.connect('bernard'),
        ]);

        expect(channels[0]).toBe(channels[1]);
      });
    });
  });
});
