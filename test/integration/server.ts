import * as http from 'http';
import socketIO from 'socket.io';

import * as c from '../../src/constants';

export default class Server {
  port: number;
  server: http.Server;
  io: any;
  addr: any;

  sockets = new Map<string, any>();
  uuidBySocketId = new Map();
  socketIdByUuid = new Map();

  constructor(port: number) {
    this.port = port;
  }

  async connect() {
    this.server = http.createServer();
    this.io = socketIO(this.server);

    this.init();

    await this.server.listen(this.port);
    this.addr = this.server.address();
  }

  async disconnect() {
    await this.server.close();
  }

  address() {
    return this.server.address();
  }

  init() {
    this.io.on('connection', this.onConnection.bind(this));
  }

  onConnection(socket) {
    this.sockets.set(socket.id, socket);

    socket.on(c.WS_SEND_JOIN, ({ from }) => {
      let clientId = from || socket.id;
      this.uuidBySocketId.set(socket.id, clientId);
      this.socketIdByUuid.set(clientId, socket.id);
    });

    socket.on(c.WS_SEND_OFFER, ({ from, offer, to }) => {
      let fromClientId = from || socket.id;
      socket.to(this.socketIdByUuid.get(to)).emit(c.WS_RECEIVE_OFFER, {
        offer,
        description: offer,
        from: fromClientId,
      });
    });

    socket.on(c.WS_SEND_ANSWER, ({ from, answer, to }) => {
      let fromClientId = from || socket.id;
      socket.to(this.socketIdByUuid.get(to)).emit(c.WS_RECEIVE_ANSWER, {
        answer,
        description: answer,
        from: fromClientId,
      });
    });

    socket.on(c.WS_SEND_ICE_CANDIDATE, ({ from, candidate, to }) => {
      let fromClientId = from || socket.id;
      socket
        .to(this.socketIdByUuid.get(to))
        .emit(c.WS_RECEIVE_ICE_CANDIDATE, { candidate, from: fromClientId });
    });

    socket.on(c.WS_SEND_LEAVE, ({ from }) => {
      let fromClientId = from || socket.id;

      socket.broadcast.emit(c.WS_RECEIVE_LEAVE, fromClientId);
    });

    socket.on('disconnect', () => {
      let clientId = this.uuidBySocketId.get(socket.id);

      if (clientId) {
        socket.broadcast.emit(c.WS_RECEIVE_LEAVE, clientId);

        this.uuidBySocketId.delete(socket.id);
        this.socketIdByUuid.delete(clientId);
      }
    });
  }
}
