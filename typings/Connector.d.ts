import { EventEmitter } from 'events';

// Unique ID for this connector
type ConnectorUUID = string | null;

// Valid supercharged message
type Message = {
  // Emitter UUID
  from?: string;
  // Receiver UUID
  to?: string;
  ts: number;
};

interface ConnectorI extends EventEmitter {
  uuid: ConnectorUUID;

  now(): number;

  on(event: string | symbol, listener: (...args: any[]) => void): this;
  emit(event: string | symbol, ...args: any[]): boolean;
  broadcast(event: string | symbol, ...args: any[]): boolean;
}
