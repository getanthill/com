import { ConnectorI } from './Connector';

// WebRTC Connector configuration
interface WebSocketConnectorConfig {
  url?: string;
  options?: object;
}

interface WebSocketConnectorI extends ConnectorI {}
