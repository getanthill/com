import { EventEmitter } from 'events';

interface ICEServer {
  username?: string;
  credential?: string;
  url: string;
  urls: string;
}

// WebRTC Connector configuration
interface WebRTCNegociatorConfig {
  iceServers?: ICEServer[];
  iceServersFetchUrl?: string;
  iceServersSyncTTLThreshold?: number;
}

interface WebRTCNegociatorI extends EventEmitter {}
