import { ConnectorI } from './Connector';
import { WebRTCNegociatorConfig } from './WebRTCNegociator';

// WebRTC Connector configuration
interface WebRTCConnectorConfig {
  serversFetchUrl?: string;
  negociator?: WebRTCNegociatorConfig;
}

interface WebRTCConnectorI extends ConnectorI {}
