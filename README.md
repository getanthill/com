# `@getanthill/com`

This library is handling the communication between clients over the
[WebRTC](https://tools.ietf.org/html/rfc7478) protocol. The
[perfect negociation in WebRTC](https://blog.mozilla.org/webrtc/perfect-negotiation-in-webrtc/)
is followed to address any race condition during the negociation between
different parts.

## Getting Started

> Check [Requirements](#requirements) for backend instructions

When your backend is up and running, you can start testing the
communication between 2 clients, let's say Alice and Bernard.

First, you need install the package:

```shell
npm install -S @getanthill/com
```

Then, the connectors must be instanciated and connected with the
following steps:

```ts
import { WebRTCConnector, WebSocketConnector } from '@getanthill/com';

async function init() {
  const uuid = 'alice';

  // WebSocket configuration and connection:
  const ws = new WebSocketConnector(uuid, {});
  await ws.connect();

  // WebRTC configuration and ready for negociation:
  const rtc = new WebRTCConnector(uuid, {}, ws);

  // Start the ICE Servers synchronization loop:
  await rtc.negociator.startIceServersSynchronization();

  // ---
  // Here, you are ready to start communicating over WebRTC!
  // For example, if you want to say `hello` when a new connection
  // is ready, you can add the following lines to your code:
  rtc.on('rtc://hello', console.log);

  rtc.on('ready', ({ to }) => {
    rtc.emit('rtc://hello', { to });
  });

  await rtc.connect('bernard');
}
```

## Requirements

This project is expecting few requirements.

### Backend

A web server must be available somewhere to expose a route providing
the ICE Servers (`GET` unauthenticated right now) and a WebSocket
server.

#### `GET:/api/servers`

Send the list of ICE Servers. The structure of the response must
respect:

```json
{
  "ttl": 86400,
  "iceServers": [
    {
      "url": "stun:server1:3478",
      "urls": "turn:server1:3478"
    },
    {
      "username": "username",
      "credential": "credential",
      "url": "turn:server1:3478",
      "urls": "turn:server1:3478"
    }
  ]
}
```

- `ttl` is a number defining the expiration of this list in seconds.
- `iceServers` is a list of ICE Servers URL. Additional resources can be found here: https://www.twilio.com/docs/stun-turn/api

#### WebSocket

A WebSocket must be configured and started to react on the events
starting with `ws://`. An example of WebSocket server implementation
can be found in the integration tests here:
[`./test/integration/server.ts`](./test/integration/server.ts)

> The full list of events is located here: `./src/constants/index.ts`

### ICE Servers

The simplest and easiest found to have working ICE Servers and
all the signaling system for WebRTC is to use a third-party service
such as [Twilio](https://www.twilio.com/docs/stun-turn/api) [^1] and
start testing the WebRTC communication between clients.

[^1]: I spent hours and days trying to deploy a TURN/STUN server with [CoTurn](https://github.com/coturn/coturn) with no luck.
